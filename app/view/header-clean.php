<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>{{config.title}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Language" content="en">
    <meta name="KeyWords" content="DAMSS, Data Analysis Methods for Software Systems, International Workshop">
    <meta name="Description" content="International Workshop for Data Analysis Methods for Software Systems">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link href="{{config.directory}}/styles/add_style" type="text/css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/3.5.1/css/bootstrap/tabulator_bootstrap4.min.css" rel="stylesheet">
    <script language="JavaScript" src="{{config.directory}}/scripts/overlib" type="text/javascript"></script>
    <script language="JavaScript" src="{{config.directory}}/scripts/scripts" type="text/javascript"></script>
    <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
  </head>
  <body topmargin="0" marginwidth="0" marginheight="0" leftmargin="0" bgcolor="#ffffff">
    <div class="container-fluid clean-header">
      <div class="header-bar"></div>
      <img src="/kdrmii/images/logo.jpg" alt="" width="244" height="100" class="d-none d-lg-block">
      <img src="/kdrmii/images/logo.jpg" alt="" width="122" height="50" class="d-none d-sm-block d-lg-none">
    </div>