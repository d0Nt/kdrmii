<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
    <title>{{config.title}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Language" content="en">
    <meta name="KeyWords" content="DAMSS, Data Analysis Methods for Software Systems, International Workshop">
    <meta name="Description" content="International Workshop for Data Analysis Methods for Software Systems">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link href="{{config.directory}}/styles/add_style" type="text/css" rel="stylesheet">
    <script language="JavaScript" type="text/javascript">
        <!--
        function dsp_photo(image, w, h) {
            window.open('images/gallery/' + image, 'photo', 'width=' + w + ',height=' + h + ',status=no,location=no,toolbar=no,menubar=no,scrollbars=no,resizable=yes,screenX=0,screenY=0');
        }
        -->
    </script>
    <script language="JavaScript" src="{{config.directory}}/scripts/overlib" type="text/javascript"></script>
    <script language="JavaScript" src="{{config.directory}}/scripts/scripts" type="text/javascript"></script>
</head>

<body topmargin="0" marginwidth="0" marginheight="0" leftmargin="0" bgcolor="#ffffff">
<table width="1024" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
    <tr>
        <td width="1024" valign="top" height="640">
            <table width="1024" height="640" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                <tr>
                    <td style="background-position: right center; background-repeat: repeat-y;" width="244" valign="top" background="{{config.directory}}/images/px.jpg">
                        <table width="244" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <td width="244" valign="top" height="239">
                                    <table width="244" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                        <tr valign="top">
                                            <td><img src="{{config.directory}}/images/logo.jpg" alt="" width="244" height="100"></td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>




                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=introduction&amp;lang=en" target="_top">INTRODUCTION</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>

                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=Aims_and_topics&amp;lang=en" target="_top">AIMS AND TOPICS</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>

                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=program&amp;lang=en" target="_top">PROGRAM</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>

                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=organizers&amp;lang=en" target="_top">ORGANIZERS</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>

                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=registration&amp;lang=en" target="_top">REGISTRATION</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>

                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=abstracts_of_damss&amp;lang=en" target="_top">ABSTRACTS</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>

                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=contacts&amp;lang=en" target="_top">CONTACTS</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>

                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=DAMSS_2017&amp;lang=en" target="_top">DAMSS 2017</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>

                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=DAMSS_2016&amp;lang=en" target="_top">DAMSS 2016</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>

                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=datamss_2015&amp;lang=en" target="_top">DAMSS 2015</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>

                                        <tr>
                                            <td class="menu_bg1" onmouseout="this.className='menu_bg1'" onmouseover="this.className='menu_bg2'" width="244" height="22">
                                                <div style="margin-left: 45px; margin-top: 4px;"><a href="https://www.mii.lt/datamss_test/index.php?page=past_workshops&amp;lang=en" target="_top">PAST WORKSHOPS</a></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#d7e2ea"></td>
                                        </tr>









                                        <tr>
                                            <td height="100%">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td alt="" width="244" valign="top" height="401">
                                    <div style="margin-left: 7px; margin-top: 11px;">
                                        <table cellspacing="0" cellpadding="1" border="0" bgcolor="#d7e2ea">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <table width="222" height="91" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                                                        <tbody>
                                                        <tr>
                                                            <td valign="top" height="22" background="{{config.directory}}/images/bg2_td_px.jpg">
                                                                <div style="height: 22px; background-image: url('{{config.directory}}/images/bg2_td.jpg'); background-position: right center; background-repeat: no-repeat">
                                                                    <div style="margin-left: 9px; padding-top: 7px;">
                                                                    </div>
                                                                </div>

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 10px 10px 10px 14px; line-height: 12px;" class="small" valign="top" height="359">
                                                                <center> 10th International Workshop<br> <br> <strong>Data Analysis Methods for Software Systems</strong><br> X DAMSS<br> Druskininkai, Lithuania,<br> Hotel „Europa Royale“,<br>                                                                                            http://www.mii.vu.lt/DAMSS<br> </center> <br> <br> <br>







                                                                <!--
<center>
9th International Workshop<br>
<br>
<strong>Data Analysis Methods for Software Systems</strong><br>
9th DAMSS<br>
Druskininkai, Lithuania,<br>
Hotel „Europa Royale“,<br>
http://www.mii.vu.lt/DAMSS<br>
</center>

<br>
<br>


<strong>Topics of the workshop</strong><br>
<img src="images/bull.jpg" alt="">&nbsp;Data Mining<br>
<img src="images/bull.jpg" alt="">&nbsp;Software Engineering<br>
<img src="images/bull.jpg" alt="">&nbsp;Visualization Methods of Multidimensional Data<br>
<img src="images/bull.jpg" alt="">&nbsp;Medical Informatics<br>
<img src="images/bull.jpg" alt="">&nbsp;Ontological Engineering<br>
<img src="images/bull.jpg" alt="">&nbsp;Business Rules<br>
-->
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="780" valign="top">
                        <div><img src="{{config.directory}}/images/6top.jpg" width="780" height="11"></div>
                        <table width="780" height="629" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <td width="769" valign="top">
                                    <div style="margin-left: 6px; margin-top: 7px;">
                                        <table cellspacing="0" cellpadding="1" border="0" bgcolor="#cbe4e1">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <table width="754" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                                                        <tbody>
                                                        <tr>
                                                            <td valign="top" height="22" background="{{config.directory}}/images/bg1_td_px.jpg">
                                                                <div style="height: 22px; background-image: url('{{config.directory}}/images/bg1_td.jpg'); background-position: right center; background-repeat: no-repeat">
                                                                    <div style="margin-left: 9px; padding-top: 7px;">
                                                                    </div>
                                                                    <div style="margin-top: 0px; margin-left: 729px; width: 20px;">
                                                                        <a href="https://www.mii.lt/datamss_test/index.php?lang=lt"><img src="{{config.directory}}/images/flag_lt.gif" border="0"></a></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 10px 10px 10px 14px; line-height: 14px;" class="normal" valign="top" height="591">
